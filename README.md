[Rendu de mon site](https://docs.forge.apps.education.fr/modeles/pyodide-mkdocs-theme-review/)

Modifier l'url du site
Ajouter des renseignements si nécessaire

## Tutoriel 

Le tutoriel pour utiliser ce modèle est ici : [tutoriel](https://docs.forge.apps.education.fr/modeles/tutoriels/pyodide-mkdocs-theme-review/)

Si vous voulez supprimer le lien vers le dépôt de votre site en haut à droite : ![lien_depot](docs/assets/images/aller_depot.png){ width=5%}   
👉 les explications sont ici : [Tutoriel : votre propre contenu](https://docs.forge.apps.education.fr/modeles/tutoriels/pyodide-mkdocs-theme-review/08_tuto_fork/1_fork_projet/#iii-votre-propre-contenu){:target="_blank" }


## Noms de groupes

La forge est un espace de liberté pour ses membres. Mais nous souhaitons attirer votre attention sur le choix du nom d'un groupe que vous décideriez de créer pour ensuite l'utiliser en _GitLab Pages_.

Si vous choisissez "Ministère" alors votre application en ligne ou site web personnel sera accessible à l'URL `ministere.forge.apps.education.fr` Cet exemple est caricatural mais vous comprenez aisément que cela puisse poser problème et créer de la confusion. De meme "Troisième", "Mathématiques" ou "Tutoriels" qui pourraient laisser à penser qu'on y retrouvera toutes les ressources de troisième et de mathématiques de la forge, ou des tutoriels concernant l'usage de la forge.

Merci donc de personnaliser et de ne pas utiliser de noms génériques pour vos groupes : soyez le plus précis possible. Par exemple, si votre lycée s'appelle Van Gogh, ne mettez pas seulement vanGogh, mais par exemple lyceeVanGoghErmont (lycée Van Gogh d'Ermont)

Nous vous demandons donc de respecter les règles suivantes :

* Utiliser un nom de groupe qui soit clair et précis, et qui ne soit pas trompeur quant à l'identité ou les objectifs du groupe.
* Ne pas choisir un nom de groupe qui pourrait créer une confusion avec un autre groupe existant ou qui pourrait nuire à la réputation d'autrui.
* Les noms de groupe ne doivent en aucun cas contenir des éléments offensants, discriminatoires, ou qui pourraient gêner d'autres utilisateurs.
* Les noms de groupe trop génériques, tels que "modèles" ou "cours", sont interdits /a priori/ afin de prévenir toute confusion et de maintenir une organisation claire au sein de la plateforme. Ils peuvent toutefois être utilisés, après accord par le comité de suivi (COSUI) s'ils représentent une communauté de contributeurs.

Le [comité de suivi](https://docs.forge.apps.education.fr/apropos.html) se réserve le droit de juger qu'un nom de groupe n'est pas approprié et de vous demander d'en changer. Merci de votre compréhension. Certains groupes aux noms génériques existent et sont liés à l'existence d'une communauté active sur le sujet.

## Les licences

Quelle licence choisir pour mon projet sur la forge ?

Commençons par rappeler que tout projet de LaForgeÉdu est un logiciel **libre** ou une ressource éducative **libre**. Et donc tout projet doit s'accompagner à la racine du dépôt d'un fichier nommé LICENSE explicitant la licence choisie (ex. [celui de MathALEA](https://forge.apps.education.fr/coopmaths/mathalea/-/blob/main/LICENSE)).

- **Si votre projet est un logiciel libre** (i.e. du code) alors il conviendra d'en choisir la licence parmi [cette liste](https://www.data.gouv.fr/fr/pages/legal/licences/) du site data.gouv. Si vous choisissez une licence _permissive_, nous vous suggérons la MIT. Si vous préférez une licence _à réciprocité_ (copyleft), nous vous suggérons la GPLv3. Liens vers le site de l'ANCT pour en savoir plus [sur les licences libres en général](https://licence-libre.incubateur.anct.gouv.fr/) et [sur la distinction importante entre les licences permissives et les licences copyleft](https://licence-libre.incubateur.anct.gouv.fr/licence-libre/le-point-sur-les-licences-libres) en particulier.

- **Si votre projet est une ressource éducative libre** (par ex. du contenu pédagogique sur un site web), il conviendra d'en choisir la licence parmi les différents types de licence Creative Commons. Nous préférons la CC-BY et nous déconseillons d'introduire la clause ND (car l'éducation est en perpétuelle évolution). Pour orienter votre choix, nous vous suggérons ces quatre lectures : le témoignage d'un enseignant [Pourquoi je publie mes travaux sous licence libre](https://ababsurdo.fr/blog/20141119-pourquoi-publier-sous-licence-libre/), ces deux articles issus de blog de la chaire RELIA de l'Unesco [Choisir une licence ouverte, une affaire de goût ou de posture ?](https://chaireunescorelia.univ-nantes.fr/2023/06/14/choisir-une-licence-ouverte-une-affaire-de-gout-ou-de-posture/) et [Comment garantir les deux “R” qui fâchent…](https://chaireunescorelia.univ-nantes.fr/2024/10/30/comment-garantir-les-deux-r-qui-fachent/) ainsi que [La connaissance libre grâce aux licences Creative Commons, ou pourquoi la clause « pas d’utilisation commerciale » ne répond pas (forcément) à vos besoins](https://upload.wikimedia.org/wikipedia/commons/0/0b/WMBE-La_connaissance_libre_gr%C3%A2ce_aux_licences_Creative_Commons.pdf) de Wikimédia Belgique.
---
author: Votre nom
title: Page SQL
---

!!! info "Lien pour la documentation"
 

    [doc pour sql](https://epithumia.github.io/mkdocs-sqlite-console/usage/#afficher-la-consoleide){:target="_blank" }



## I. Ecrire votre propre code, puis l'exécuter


{!{ sqlide titre="Votre code SQL" espace="exercices_sql"}!}

## II. Du code pré-rempli : 

{!{ sqlide titre="tester SQL avec du code pré-saisi" sql="avec_SQL/sql/code_1.sql" espace="exercices_sql"}!}

!!! danger "Attention"

    Par défaut SQLite ne vérifie pas l’intégrité des contraintes de clef étrangères. Il faut lui dire explicitement de le faire avec `PRAGMA foreign_keys=1;`  

## III. Initialisation d'un IDE avec du code caché :


Code du fichier caché donné par `init=` : 

```sql title="ce qui est caché"
-- Pour la vérification de l'intégrité des contraintes de clé étrangère : 
PRAGMA foreign_keys=1;

-- Pour créer la table
DROP TABLE IF EXISTS employees;
CREATE TABLE employees
(
    id          integer,
    name        text,
    designation text,
    manager     integer,
    hired_on    date,
    salary      integer,
    commission  float,
    dept        integer
);

INSERT INTO employees VALUES (1,'JOHNSON','ADMIN',6,'1990-12-17',18000,NULL,4);
INSERT INTO employees VALUES (2,'HARDING','MANAGER',9,'1998-02-02',52000,300,3);
INSERT INTO employees VALUES (3,'TAFT','SALES I',2,'1996-01-02',25000,500,3);
INSERT INTO employees VALUES (4,'HOOVER','SALES I',2,'1990-04-02',27000,NULL,3);
INSERT INTO employees VALUES (5,'LINCOLN','TECH',6,'1994-06-23',22500,1400,4);
INSERT INTO employees VALUES (6,'GARFIELD','MANAGER',9,'1993-05-01',54000,NULL,4);
```


{!{ sqlide titre="init + code" init="avec_SQL/sql/init_1.sql" sql="avec_SQL/sql/code_sql.sql" espace="exercices_sql" }!}


## IV. Utilisation d'une base de donnée : 


Base et init ne peuvent pas être utilisés en même temps

Le code caché auto-exécuté indique la base de donnée utilisée `livres`, et le code `PRAGMA foreign_keys=1;`  

{!{sqlide titre="avec base Livres" base="avec_SQL/bases/Livres.db" sql="avec_SQL/sql/option.sql" espace="mediatheque" autoexec hide}!}

La ligne ci-dessus montre que le code caché s'est bien exécuté.

!!! example "Exemple"

    Combien de livres contiennent la chaîne "Astérix" dans leur titre ?

    {!{ sqlide titre="" sql="avec_SQL/sql/asterix.sql" espace="mediatheque"}!}


!!! example "Peut-on supprimer n’importe quelle ligne ?"

    Essayons de supprimer le livre "Hacker's Delight" sachant que son code ISBN est 978-0201914658

    {!{ sqlide titre="" sql="avec_SQL/sql/exemple.sql" espace="mediatheque"}!}

    Le SGBD nous indique que supprimer ce livre, violerait la contrainte de clé étrangère. En effet, le code isbn est une clé étrangère dans la table `auteur_de`.


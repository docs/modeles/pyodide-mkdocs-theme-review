# --- PYODIDE:env --- #

# Un import de matplotlib en tout premier est indispensable, pour que la classe
# PyodidePlot devienne disponible dans l'environnement:
import matplotlib.pyplot as plt
PyodidePlot().target()     # Cible la figure dans laquelle tracer la figure dans la page

# --- PYODIDE:code --- #
# L'import suivant a été fait dans du code caché : 
# import matplotlib.pyplot as plt
xs = [-3 + k * 0.1 for k in range(61)]
ys = [x**2 for x in xs]
plt.plot(xs, ys, "r-")
plt.grid()  # Optionnel : pour voir le quadrillage
plt.axhline()  # Optionnel : pour voir l'axe des abscisses
plt.axvline()  # Optionnel : pour voir l'axe des ordonnées
plt.title("La fonction carré")
plt.show()

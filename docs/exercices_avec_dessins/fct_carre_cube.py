# --- PYODIDE:env --- #

import matplotlib.pyplot as plt
fig1 = PyodidePlot('cible_1')  
fig2 = PyodidePlot('cible_2')

# --- PYODIDE:code --- #
# L'import suivant a été fait dans du code caché : 
# import matplotlib.pyplot as plt

fig1.target() # Pour tracer la fonction ci-dessous
xs = [-3 + k * 0.1 for k in range(61)]
ys = [x**2 for x in xs]
plt.plot(xs, ys, "r-")
plt.grid()  # Optionnel : pour voir le quadrillage
plt.axhline()  # Optionnel : pour voir l'axe des abscisses
plt.axvline()  # Optionnel : pour voir l'axe des ordonnées
plt.title("La fonction carré")
plt.show()

fig2.target() # Pour tracer la fonction ci-dessous
xs = [-2 + k * 0.1 for k in range(41)]
ys = [x**3 for x in xs]
plt.plot(xs, ys, "r-")
plt.grid()  # Optionnel : pour voir le quadrillage
plt.axhline()  # Optionnel : pour voir l'axe des abscisses
plt.axvline()  # Optionnel : pour voir l'axe des ordonnées
plt.title("La fonction cube")
plt.show()

---
author: Mireille Coilhac
title: Images  en Python
---

😊

## I. Utiliser la bibliothèque matplotlib 

### La fonction carré

{{ IDE('fct_carre') }}

{{ figure() }}

### Les courbes de la fonction carré et de la fonction cube séparées

{{ IDE('fct_carre_cube') }}
 
{{ figure('cible_1') }}

{{ figure('cible_2') }}

### Les courbes de la fonction carré et de la fonction cube superposées

{{ IDE('carre_cube_superposees') }}

{{ figure('cible_double') }}



## II. Utiliser la tortue par Romain Janvier


???+ question "Utilisation de la tortue"

    {{ IDE('arbre_tortue') }}

    {{ figure('cible_3') }}




---
author: Votre nom
title: 🏡 Accueil
---

À vous de personnaliser cet accueil

!!! info "Adapter ce site modèle"

    Le tutoriel est ici : [Tutoriel de site avec python](https://docs.forge.apps.education.fr/modeles/tutoriels/pyodide-mkdocs-theme-review/){:target="_blank" }
    
    Si vous voulez conserver certaines pages de ce modèles sans qu'elles ne soient visibles dans le menu, il suffit de les enlever du fichier .pages   
    Vous les retrouverez facilement en utilisant la barre de recherche en haut à droite

    Si vous voulez supprimer le lien vers le dépôt de votre site en haut à droite : ![lien_depot](assets/images/aller_depot.png){ width=5%}   
    👉 les explications sont ici : [Tutoriel : votre propre contenu](https://docs.forge.apps.education.fr/modeles/tutoriels/pyodide-mkdocs-theme-review/08_tuto_fork/1_fork_projet/#iii-votre-propre-contenu){:target="_blank" }
    

😊  Bienvenue !

_Dernière MAJ le 11/03/2025_





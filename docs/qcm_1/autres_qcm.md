---
author: Mireille
title: Ecrire des QCM
tags:
  - qcm
  - Difficulté **
---

Pour créer **très facilement de façon automatique** ce fichier, suivre ce lien : [Création de QCM : générer le fichier .json automatiquement](https://frederic-zinelli.gitlab.io/pyodide-mkdocs-theme/redactors/qcm_builder/){ .md-button target="_blank" rel="noopener" }. 

{{ multi_qcm('qcm_exemple.json') }}

